package com.example.oktay.besharf;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by oktay on 10.11.2015.
 */
public class UserScoreAdaptor extends ArrayAdapter<UserScore> {
    private Context context;
    private int resourceLayoutId;
    private List<UserScore> scoreList;

    public UserScoreAdaptor(Context context, int resource, List<UserScore> objects) {
        super(context, resource, objects);
        this.context =context;
        this.resourceLayoutId = resource;
        this.scoreList = objects;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        userViewHolder holder = null;


        if (row==null){
            LayoutInflater inflater = LayoutInflater.from(context);


            row = inflater.inflate(resourceLayoutId,parent,false);


            holder = new userViewHolder();
            holder.indexView = (TextView)row.findViewById(R.id.s_index);
            holder.scoreView = (TextView)row.findViewById(R.id.s_score);
            holder.nameView = (TextView)row.findViewById(R.id.s_name);

            row.setTag(holder);
        }else {
            holder = (userViewHolder)row.getTag();
        }

        UserScore userScore = scoreList.get(position);
        holder.scoreView.setText(""+userScore.getScore());
        holder.indexView.setText(""+userScore.getIndex());
        holder.nameView.setText(""+userScore.getUserName());

        return row;
    }

    static class userViewHolder{
        TextView scoreView;
        TextView indexView;
        TextView nameView;
    }
}
