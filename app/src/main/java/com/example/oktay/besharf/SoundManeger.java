package com.example.oktay.besharf;

import android.media.AsyncPlayer;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;

/**
 * Created by oktay on 18.10.2015.
 */
public class SoundManeger {

    private static AsyncPlayer player;
    private static AsyncPlayer ticPlayer;
    private static MediaPlayer mPlayer;

    SoundManeger() {
        // mPlayer= MediaPlayer.create(Constant.getActivity(), R.raw.bubble_bath);
    }

    public static AsyncPlayer getPlayer() {
        return player;
    }

    public static void setmPlayer(MediaPlayer mPlayer) {
        SoundManeger.mPlayer = mPlayer;
    }

    public static void play(int i) {

        if(player ==null){
            player = new AsyncPlayer("pl");
        }
       if (Constant.isSoundOn()){
           Uri alert = Uri.parse("android.resource://" + Constant.getActivity().getPackageName() +"/"+ i);
           player.play(Constant.getActivity(), alert, false, AudioManager.STREAM_MUSIC);
       }

    }

    public static MediaPlayer getmPlayer() {
        return mPlayer;
    }

    public static void play(String i) {

        if(ticPlayer ==null){
            ticPlayer = new AsyncPlayer("tic");
        }

        if (Constant.isSoundOn()){
            Uri alert = Uri.parse("android.resource://" + Constant.getActivity().getPackageName() +"/"+ R.raw.tic);
            ticPlayer.play(Constant.getActivity(), alert, true, AudioManager.STREAM_MUSIC);

        }
    }
    public static void stopTicPlayer(){
        ticPlayer.stop();
    }




}
