package com.example.oktay.besharf;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.duman.ingilizceoyunu.NotiReciver;
import com.duman.ingilizceoyunu.WebDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class UserActivity extends Activity {


    private static final String KEY_ANDROID_ID = "aid";
    private static final String KEY_FIRST_NAME = "first_name";
    private static final String KEY_NICK_NAME = "nick_name";
    private static final String KEY_GAME_OBJECT = "gameObject";
    private static Dialog dialog;


    private static CountDownTimer timer;
    private static JSONObject userObject;
    private static Button saveButton, gameStartButton;
    private static String android_id,dialogTitle;
    private TextView nickNameTv;
    private TextView timeTv,secondTv;

    private static List<UserScore> userScoreList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        nickNameTv = (TextView) findViewById(R.id.user_nick_name_text);
        timeTv = (TextView) findViewById(R.id.tv_time);
        gameStartButton = (Button) findViewById(R.id.game_start);
        secondTv = (TextView)findViewById(R.id.textView_second);
        timeTv.setGravity(Gravity.CENTER);
        new Constant(this);

        android_id = Settings.Secure.getString(Constant.getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY,22);
        calendar.set(Calendar.MINUTE,00);
        calendar.set(Calendar.SECOND,00);


        Intent in= new Intent(this, NotiReciver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this,0,in,0);
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);


        if(Calendar.getInstance().getTimeInMillis()>calendar.getTimeInMillis()){
            alarmManager.cancel(pendingIntent);
            calendar.add(Calendar.DAY_OF_WEEK,1);

            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),pendingIntent);
        }else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),pendingIntent);
        }


    }


    @Override
    protected void onResume() {
        super.onResume();
        Constant.setActivity(this);
        fullScreencall();
        //new sendJSON().execute();
        userScoreList = new ArrayList<>();


        JSONObject o = new JSONObject();
        try {
            o.accumulate(KEY_ANDROID_ID, android_id);
            getJson(o);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    void getScoreList(String url){
        JsonObjectRequest request = new JsonObjectRequest(Constant.getURL() +"yuksekskor?tur="+ url, null,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {


                        if (response != null) {
                            try {
                                JSONArray array = response.getJSONArray("scorelist");
                                userScoreList.clear();
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject o = (JSONObject) array.get(i);
                                    userScoreList.add(new UserScore(i+1,o.getString("userName"), o.getInt("score")));

                                    System.out.println(" l  " + o.toString());
                                }

                                Comparator<UserScore> comparator = new Comparator<UserScore>() {
                                    @Override
                                    public int compare(UserScore lhs, UserScore rhs) {
                                        return rhs.getScore()-lhs.getScore();
                                    }
                                };


                                Collections.sort(userScoreList,comparator);

                                for (int i = 0; i <userScoreList.size() ; i++) {
                                    userScoreList.get(i).setIndex(i+1);

                                }
                                createList();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        ApplicationController.getsInstance().addToRequestQueue(request);

    }

    void createList() {
        dialog = new Dialog(this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.scorelist);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView tv = (TextView) dialog.findViewById(R.id.textView_tit);

        tv.setText(dialogTitle);
        ListView lv = (ListView) dialog.findViewById(R.id.scorelist);
        lv.setBackgroundColor(Color.TRANSPARENT);
        UserScoreAdaptor arrayAdapter = new UserScoreAdaptor(UserActivity.this,R.layout.user_score_view,userScoreList);
        lv.setAdapter(arrayAdapter);
        dialog.show();
    }

    /*
    *
    * android idi sini gönderip sonuçları ladığım zımbırtı
    *
    * Eğer kullanıcı sunucuda yoksa yeni oluşurur
    *
    * */

    public void getJson(JSONObject o) {


        JsonObjectRequest request = new JsonObjectRequest(Constant.getURL() + "kullaniciekle", o,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        userObject = response;
                        postExecute();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }


        );
        ApplicationController.getsInstance().addToRequestQueue(request);

    }


    public void fullScreencall() {
        if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) { // lower api
            View v = this.getWindow().getDecorView();
            v.setSystemUiVisibility(View.GONE);
        } else if (Build.VERSION.SDK_INT >= 19) {
            //for new api versions.
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();

        if (timer != null) {
            timer.cancel();
        }
    }


    public void click(View v) {

        switch (v.getId()) {
            case R.id.game_start:
                startActivity(new Intent(this, MainActivity.class));                                //oyunu başlat
                break;
            /*
            *
            * kullanıcının isim ve diğer
            * bilgilerini girecek dialoğu göster
            * */

            case R.id.user_nick_name_text:
                showDialog();
                break;
            case R.id.setting:
                showDialog();
                break;
            case R.id.week_high_scr_list:
                dialogTitle = getResources().getString(R.string.wek_btn);
                getScoreList("week");

                break;
            case R.id.day_high_scr_list:
                dialogTitle = getResources().getString(R.string.day_ligh_score_list);
                getScoreList("day");
                break;
            case R.id.f_whatsap:
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.duman.ingilizceoyunu");
                intent.setPackage("com.whatsapp");
                startActivity(intent);
                break;

            case R.id.f_share:
                String shareBody = "https://play.google.com/store/apps/details?id=com.duman.ingilizceoyunu";
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, ""));
            break;

            case R.id.f_play:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.duman.ingilizceoyunu")));

                break;

            case R.id.f_fa:
                startActivity(getOpenFacebookIntent(this));
                break;
            case R.id.f_question:
                new WebDialog(this).show();
                break;

        }
    }

    public static Intent getOpenFacebookIntent(Context context) {

        try {
            context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/1668492673391527"));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/dilbaz.kelimeoyunu/"));
        }
    }


    public void showDialog() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.user_edit);
        dialog.setTitle("Profil");

        saveButton = (Button) dialog.findViewById(R.id.user_button);
        Button cancel =(Button)dialog.findViewById(R.id.user_button_disable);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        final EditText nText = (EditText) dialog.findViewById(R.id.ed_user_name);
        final EditText nickText = (EditText) dialog.findViewById(R.id.ed_user_nick_name);

        try {
            nickText.setText("" + userObject.getString(KEY_NICK_NAME));
            nText.setText("" + userObject.getString(KEY_FIRST_NAME));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                JSONObject o = new JSONObject();
                try {

                    o.accumulate(KEY_ANDROID_ID, android_id);
                    o.accumulate(KEY_FIRST_NAME, "" + nText.getText().toString());
                    o.accumulate(KEY_NICK_NAME, "" + nickText.getText().toString());
                    getJson(o);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
       // dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);

//Show the dialog!
        dialog.show();

//Set the dialog to immersive

//Clear the not focusable flag from the window
        //dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }


    void postExecute() {

        if (userObject != null) {
            try {
                nickNameTv.setText("" + userObject.getString(KEY_NICK_NAME).toUpperCase());
                JSONObject gameObject = userObject.getJSONObject(KEY_GAME_OBJECT);
                Constant.setGameObject(gameObject);
                //timeTv.setText("" + gameObject.getInt("time"));


                int t = gameObject.getInt("time") - 20;

                if (t <= 0) {

                    t = t + 20;
                    gameStartButton.setVisibility(View.INVISIBLE);
                    //// TODO: 19.10.2015 oyunu bekleyeceek algoritmayayı yaz
                    timer = new CountDownTimer(t * 1000, 1000) {
                        @Override
                        public void onTick(long l) {
                            timeTv.setText(("Yeni oyunun başlamasına  \n" +
                                    "").toUpperCase());
                            secondTv.setText(""+l/1000);

                        }

                        @Override
                        public void onFinish() {
                            gameStartButton.setVisibility(View.VISIBLE);
                            startActivity(new Intent(UserActivity.this, MainActivity.class));

                        }
                    };

                } else {
                    // TODO: 19.10.2015 oyunn devam ediyor başla
                    timer = new CountDownTimer(t * 1000, 1000) {
                        @Override
                        public void onTick(long l) {
                            timeTv.setText("oyun devam ediyor".toUpperCase());
                            secondTv.setText(""+l/1000);

                        }

                        @Override
                        public void onFinish() {
                            JSONObject o = new JSONObject();
                            try {
                                o.accumulate(KEY_ANDROID_ID, android_id);
                                getJson(o);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    };
                }

                timer.start();
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


    }


}
