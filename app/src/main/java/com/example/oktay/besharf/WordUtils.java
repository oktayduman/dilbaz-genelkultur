package com.example.oktay.besharf;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by oktay on 04.10.2015.
 */
public class WordUtils {

    public static List<String> suffleList(List<String> list){
        List<String> tempList = new ArrayList<>();
        Random random = new Random();
        for(int i=0;i<list.size();i++){
            int k= random.nextInt(list.size());
            String item= list.get(i);
            list.set(i,list.get(k));
            list.set(k,item);
        }

        tempList= list;
        return tempList;
    }
}
