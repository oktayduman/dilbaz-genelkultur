package com.example.oktay.besharf;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AsyncPlayer;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oktay on 02.10.2015.
 */
public class WordButton extends Button {

    private String letter;
    private int layoutId;
    private Button highButton;
    private static List<Button> highButtons;
    int w;
    private static boolean passed;

    private static Typeface roboTypeface;

    public WordButton(final Context context) {
        super(context);
        this.layoutId = 0;
        this.setBackgroundResource(R.drawable.alt);
        this.setTextAppearance(context,android.R.style.TextAppearance_Large);
        this.setGravity(Gravity.CENTER);


        ///this.setTypeface(this.getTypeface(), Typeface.BOLD);

        if (highButtons==null){
            highButtons = new ArrayList<>();
        }

        if(roboTypeface== null){
            roboTypeface = Typeface.createFromAsset(context.getAssets(),"fonts/robom.ttf");
        }



        this.setTypeface(roboTypeface);


        passed = false;

        highButton = new Button(context);
        w = Constant.getWidth() / Constant.getSecretString().length();
        int k = Constant.getHeight() / 6;
        if (w > k)
            w = k;

        highButton.setTypeface(roboTypeface);

        Double d = Double.valueOf(w*2);
        final LinearLayout.LayoutParams ust_param = new LinearLayout.LayoutParams(w,w);



        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layoutId == 0) {





                    highButton.setText(letter.toUpperCase());
                    highButton.setTextAppearance(context,R.style.boldText);
                    highButton.setGravity(Gravity.CENTER);

                    highButton.setBackgroundResource(R.drawable.alt);
                    highButtons.add(highButton);
                    SoundManeger.play(R.raw.c20);
                    setLetter("");
                    setLayoutId(1);
                    MainActivity.getOrtaLayout().addView(highButton, ust_param);
                    System.out.println(" secret " + Constant.getSecretString());
                    System.out.println(" üst " + ustSozcuk());
                    WordButton.this.setClickable(false);

                    WordButton.this.setBackgroundResource(R.drawable.alt_yuvarlak_back);

                    highButton.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SoundManeger.play(R.raw.c28);
                            int i= highButtons.indexOf(highButton);
                            highButtons.remove(i);
                            MainActivity.setClickCounter(0);
                            setLetter(highButton.getText().toString());

                            WordButton.this.setBackgroundResource(R.drawable.alt);
                            layoutId = 0;
                            WordButton.this.setClickable(true);
                            MainActivity.getOrtaLayout().removeView(highButton);


                        }
                    });

                }
            }
        });

    }


    public static Typeface getRoboTypeface() {
        return roboTypeface;
    }

    public static List<Button> getHighButtons() {
        return highButtons;
    }

    public static String ustSozcuk() {

        String sStr = "";
        for (Button b : highButtons) {
            sStr += b.getText().toString();
        }

        if (sStr.toUpperCase().equalsIgnoreCase(Constant.getSecretString())) {
            Toast.makeText(Constant.getActivity(), "Doğru", Toast.LENGTH_SHORT).show();
            Constant.pass = true;
            MainActivity.getOrtaLayout().removeAllViews();
            SoundManeger.play(R.raw.c26);
            passed = true;
            highButtons.clear();
            System.gc();
        } else {
            passed= false;
            if ((sStr.toLowerCase().equalsIgnoreCase(Constant.getSecretString().toLowerCase()))) {
                Toast.makeText(Constant.getActivity(), "Doğru 2", Toast.LENGTH_SHORT).show();
                Constant.pass = true;
                MainActivity.getOrtaLayout().removeAllViews();
                passed = true;
                highButtons.clear();
                System.gc();
            }
        }

        return sStr;
    }
    public static void emulateClick(int l){
        List<String> array = Word.stringToList(Constant.getSecretString().toUpperCase());
        String selectedLetter = array.get(l);

        for (int i = 0; i <MainActivity.getAltLayout().getChildCount() ; i++) {
            Button btn = (Button) MainActivity.getAltLayout().getChildAt(i);

            if (btn.getText().toString().equalsIgnoreCase(selectedLetter)){
               btn.performClick();
                break;
            }

        }
    }

    public static boolean isPassed() {
        return passed;
    }




    public int getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(int layoutId) {
        this.layoutId = layoutId;
    }

    public void setLetter(String letter) {
        this.letter = letter.toUpperCase();
        this.setText("" + letter.toUpperCase());
    }
}
