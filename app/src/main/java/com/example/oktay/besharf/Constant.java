package com.example.oktay.besharf;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.DisplayMetrics;

import org.json.JSONObject;

/**
 * Created by oktay on 02.10.2015.
 */
public class Constant {

    private static final String KEY_SOUND_ON = "sesmes";
    private static String secretString;
    private static Activity activity;
    private static final String URL = "http://dilbaz2-oktayduman.rhcloud.com/besharf/";
    private static int width;
    public static boolean pass;
    private static int height;
    private static JSONObject gameObject;
    private static String android_id;

    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;
    private  static boolean soundOn;

    Constant(Activity activity){
        pass=false;
        this.activity = activity;
        secretString ="AŞK";
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
         height = displaymetrics.heightPixels;
         width = displaymetrics.widthPixels;

        android_id = Settings.Secure.getString(Constant.getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        preferences = activity.getSharedPreferences("es", Context.MODE_PRIVATE);
        editor = preferences.edit();
    }


    public static boolean isSoundOn() {
        soundOn = preferences.getBoolean(KEY_SOUND_ON,true);
        return soundOn;
    }

    public static void setSoundOn(boolean soundOn) {

        Constant.soundOn = soundOn;
        editor.putBoolean(KEY_SOUND_ON,soundOn);
        editor.commit();
    }

    public static String getAndroid_id() {
        return android_id;
    }

    public static JSONObject getGameObject() {
        return gameObject;
    }

    public static void setGameObject(JSONObject gameObject) {
        Constant.gameObject = gameObject;
    }

    public static Activity getActivity() {
        return activity;
    }

    public static void setActivity(Activity activity) {
        Constant.activity = activity;
    }

    public static String getSecretString() {
        return secretString;
    }

    public static void setSecretString(String secretString) {
        Constant.secretString = secretString.toUpperCase();
    }

    public static String getURL() {
        return URL;
    }



    public static int getHeight() {
        return height;
    }

    public static int getWidth() {
        return width;
    }
}
