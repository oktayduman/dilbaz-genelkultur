package com.example.oktay.besharf;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

/**
 * Created by oktay on 23.10.2015.
 */
public class ApplicationController extends Application{

    public static final String TAG = ApplicationController.class.getSimpleName();

    /*
    * Global request Queue
    * */

    private RequestQueue mRequestQueue;

    private static JSONObject gameObject;

    private static ApplicationController sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

    public static synchronized ApplicationController getsInstance(){
        return sInstance;
    }

    public RequestQueue getmRequestQueue() {

        if (mRequestQueue== null){
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }


    public <T>  void addToRequestQueue(Request <T> req,String tag){
        req.setTag(TextUtils.isEmpty(tag) ? TAG:tag);

        VolleyLog.e("Kuyruuğa eklendi  " + req.getUrl());

        getmRequestQueue().add(req);
    }


    /**
     * Adds the specified request to the global queue using the Default TAG.
     *
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(TAG);

        getmRequestQueue().add(req);
    }

    /**
     * Cancels all pending requests by the specified TAG, it is important
     * to specify a TAG so that the pending/ongoing requests can be cancelled.
     *
     * @param tag
     */
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public static JSONObject getGameObject() {
        return gameObject;
    }

    public static void setGameObject(JSONObject gameObject) {
        ApplicationController.gameObject = gameObject;
    }
}
