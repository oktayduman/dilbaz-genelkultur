package com.example.oktay.besharf;

/**
 * Created by oktay on 09.10.2015.
 */
public class UserScore {

    private String userName;
    private int score;
    private int index;

    public UserScore(int i,String userName, int score) {
        this.index =i;
        this.userName = userName;
        this.score = score;
    }

    @Override
    public String toString() {
        return getIndex()+".    "+getUserName() +"    "+getScore();
    }


    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
