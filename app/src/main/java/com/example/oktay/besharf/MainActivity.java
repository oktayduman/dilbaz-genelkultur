package com.example.oktay.besharf;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class MainActivity extends Activity implements TextToSpeech.OnInitListener{

    private static final boolean DEBUG = true;
    private static LinearLayout ustLayout, ortaLayout, altLayout;
    private static ArrayList<Word> wordList;
    private static CountDownTimer timer;
    private static int w, i, puan;
    private TextView txt, infoView, puanView;
    private static final String KEY_ANDROID_ID = "aid";
    private static final String KEY_PUAN = "puan";

    private static int vPuan,clickCounter,bilinen;
    private static TextToSpeech tts;
    private static boolean alive;
    private Dialog dialog;

    private static List<UserScore> userScoreList;
    private static Button vpButton,harfAlButton;
    private static ImageView vi;
    private  Flip3dAnimation rotation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tts = new TextToSpeech(this, this);
        wordList = new ArrayList<>();
        new Constant(this);
        Constant.setActivity(this);

        i = 0;
        new WordButton(this);

        ustLayout = (LinearLayout) findViewById(R.id.ust);
        ortaLayout = (LinearLayout) findViewById(R.id.orta);
        altLayout = (LinearLayout) findViewById(R.id.alt);
        vpButton = (Button)findViewById(R.id.vp_tv);
        vi= (ImageView)findViewById(R.id.switch1);
        txt = (TextView) findViewById(R.id.time_id);
        infoView = (TextView) findViewById(R.id.info);
        puanView = (TextView) findViewById(R.id.puantv);
        harfAlButton = (Button)findViewById(R.id.harflutfenbtn);


        userScoreList = new ArrayList<>();
        if (timer !=null){
            timer.cancel();
        }

        fullScreenCall();
        toZero();
        dialog = new Dialog(Constant.getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);


        if(Constant.isSoundOn()){
            vi.setBackgroundResource(R.drawable.unmute);
            //new SoundManeger();   ses ekleneceiği zaman kullanılacak
            //SoundManeger.getmPlayer().start();
        }else{
            vi.setBackgroundResource(R.drawable.mute);
        }




        /*
        *
        * Sunucudan listeyi iste
        * */


        getWordListFromServer();
    }

    public void fullScreenCall() {
        if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) { // lower api
            View v = this.getWindow().getDecorView();
            v.setSystemUiVisibility(View.GONE);
        } else if (Build.VERSION.SDK_INT >= 19) {
            //for new api versions.
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }


    public static ArrayList<Word> getWordList() {
        return wordList;
    }


    public static int getI() {
        return i;
    }

    @Override
    protected void onResume() {
        super.onResume();
        alive = true;
        fullScreenCall();
        Constant.setActivity(this);
        log("on Resume");
        if(tts==null){
            tts = new TextToSpeech(this, this);
        }


    }

    private void toZero(){
        i = 0;
        puan =0;
        wordList.clear();
        clearData();
        log("to zero");

    }

    @Override
    protected void onPause() {
        super.onPause();
        alive = false;
    }

    @Override
    protected void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }


        if (timer != null) {
            timer.cancel();
        }
        log("ondestraydayımmm ");
        super.onDestroy();
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.getDefault());

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
               // Toast.makeText(this,"sıkıntı va 1",Toast.LENGTH_LONG).show();
                //Log.e("TTS", "This Language is not supported");
            } else {
                //speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
           // Toast.makeText(this,"Lütfen Google Konuşma ",Toast.LENGTH_LONG).show();
        }


    }
    private void speakOut(String s) {
        if(alive && Constant.isSoundOn()){
            tts.speak(s, TextToSpeech.QUEUE_FLUSH, null);
        }
    }





    /*
    *
    * android idi sini gönderip sonuçları ladığım zımbırtı
    *
    * Eğer kullanıcı sunucuda yoksa yeni oluşurur
    *
    * */

    public void getJson(JSONObject o) {

        dialog.setContentView(R.layout.progressdialog);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);


        //Show the dialog!
        dialog.show();

        //Set the dialog to immersive
        dialog.getWindow().getDecorView().setSystemUiVisibility(
                Constant.getActivity().getWindow().getDecorView().getSystemUiVisibility());

        //Clear the not focusable flag from the window
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        clearData();
        JsonObjectRequest request = new JsonObjectRequest(Constant.getURL() + "skorekle", o,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        ApplicationController.getsInstance().addToRequestQueue(request);

    }



    public void clearData() {

        WordButton.getHighButtons().clear();
        ustLayout.removeAllViews();
        ortaLayout.removeAllViews();
        altLayout.removeAllViews();
        System.gc();
    }

    public void click(View v) {
        switch (v.getId()) {

            case R.id.next:
                if(i<wordList.size() && i>0){
                createGame();
                }else{
                    speakOut(" Sen chok yanlish gelmishsin kardesh!");
                    Toast.makeText(Constant.getActivity(),"Listenin sonundasın:)".toUpperCase(),Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.pre:
                if(wordList.size()>0 && i>1){
                    i=i-2;
                    createGame();
                }else{
                    speakOut(" Sen chok yanlish gelmishsin kardeshhhh!");
                    Toast.makeText(Constant.getActivity(),"Listenin başındasın :)".toUpperCase(),Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.harflutfenbtn:

                int fark =(Constant.getSecretString().length()-ortaLayout.getChildCount()-1);

                if(clickCounter==0 && ortaLayout.getChildCount()>0){
                    i--;
                    createGame();
                }else {
                    if (fark>=0 && clickCounter<Constant.getSecretString().length()){

                        vPuan=fark*3;
                        WordButton.emulateClick(clickCounter);

                        clickCounter++;
                        vpButton.setText("+" + vPuan);
                    }
                }


                break;
            case R.id.switch1:

                if (Constant.isSoundOn()){
                    vi.setBackgroundResource(R.drawable.mute);
                    Constant.setSoundOn(false);

                }else {
                    vi.setBackgroundResource(R.drawable.unmute);
                    Constant.setSoundOn(true);
                }


        }
    }


    public void flip(){
       if(ustLayout.getChildCount()>0){
           Random  random = new Random();
           int i= random.nextInt(ustLayout.getChildCount());
           View v= ustLayout.getChildAt(i);
            rotation = new Flip3dAnimation(v);
           rotation.applyPropertiesInRotation();
           v.startAnimation(rotation);

       }
    }

    public static void setClickCounter(int clickCounter) {
        MainActivity.clickCounter = clickCounter;
    }

    void createGame() {
        clickCounter=0;

        harfAlButton.setText("+Harf");
        clearData();



        System.out.println(" create game");
        Constant.pass = false;
        if (i == wordList.size()) {
            i = 0;
        }
        Word word = wordList.get(i);
        i++;

        List<String> temp = Word.stringToList(word.getTur());

        for (int i = 0; i <5 ; i++) {
            temp = WordUtils.suffleList(temp);
        }

        List<String> turStringList = WordUtils.suffleList(temp);

        //List<String> engStringList = Word.stringToList(word.getEng());
        Constant.setSecretString(word.getTur());
        w = Constant.getWidth() / word.getTur().length()+1;
        int k = Constant.getHeight() / 5;
        if (w > k)
            w = k;

        int fark =(Constant.getSecretString().length());

        vPuan=fark*3;
        vpButton.setText("+"+vPuan);

        infoView.setText("" + bilinen + "/" + i + "/" + wordList.size());
        puanView.setText("" + puan);

        Flip3dAnimation rotation = new Flip3dAnimation(puanView);
        rotation.applyPropertiesInRotation();
        puanView.startAnimation(rotation);

        //kelimeleri alt layouta dizme kısmı
        final LinearLayout.LayoutParams ust_param = new LinearLayout.LayoutParams(w, w);
        for (String s : turStringList) {
            final WordButton button = new WordButton(this);
            button.setTextSize(w/7);
            button.setLetter(s);
            altLayout.addView(button, ust_param);
        }


        w = (Constant.getWidth()-200) / word.getEng().length();

        if (w > k) {
            w = k;
        }
        speakOut(wordList.get(i-1).getEng());
        View.OnClickListener v= new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                speakOut(wordList.get(i - 1).getEng());
            }
        };

        TextView view = new TextView(this);
        view.setTextColor(Color.WHITE);
        int p=10;
        view.setPadding(p,p,p,p);
        view.setBackgroundColor(Color.parseColor("#833000"));
        view.setTextSize(20);
        view.setGravity(Gravity.CENTER);
        view.setText(""+word.getEng());
        ustLayout.addView(view);

        System.gc();



    }



    public static LinearLayout getOrtaLayout() {
        return ortaLayout;
    }


    void getScoreList(){
        JsonObjectRequest request = new JsonObjectRequest(Constant.getURL() + "liste", null,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {


                        if (response != null) {
                            try {
                                JSONArray array = response.getJSONArray("scorelist");
                                userScoreList.clear();
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject o = (JSONObject) array.get(i);
                                    userScoreList.add(new UserScore(0,o.getString("userName"), o.getInt("score")));

                                    System.out.println(" l  " + o.toString());
                                }

                                Comparator<UserScore> comparator = new Comparator<UserScore>() {
                                    @Override
                                    public int compare(UserScore lhs, UserScore rhs) {
                                        return rhs.getScore()-lhs.getScore();
                                    }
                                };


                                Collections.sort(userScoreList,comparator);

                                for (int i = 0; i <userScoreList.size() ; i++) {
                                    userScoreList.get(i).setIndex(i+1);

                                }
                                createList();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        ApplicationController.getsInstance().addToRequestQueue(request);

    }




    void createList() {
        dialog.setContentView(R.layout.scorelist);

        ListView lv = (ListView) dialog.findViewById(R.id.scorelist);
        UserScoreAdaptor arrayAdapter = new UserScoreAdaptor(this,R.layout.user_score_view,userScoreList);
        lv.setAdapter(arrayAdapter);
        //dialog.show();
    }



    private  static  void log(String s){

        if(DEBUG){
            Log.i(Constant.getActivity().getClass().getSimpleName(),s);
        }
    }


    void getWordListFromServer(){


        JsonObjectRequest request = new JsonObjectRequest(Constant.getURL() + "kelimeler", null,

                new Response.Listener<JSONObject>() {
                    int sec;

                    @Override
                    public void onResponse(JSONObject response) {

                        /**
                         *
                         * Cevvap geldiğinde oyunun can alacı noktası
                         *
                         * */


                        if (response != null) {
                            clearData();
                            toZero();
                            try {

                                JSONArray arrayJs = response.getJSONArray("list");
                                for (int i = 0; i < arrayJs.length(); i++) {
                                    JSONObject o = (JSONObject) arrayJs.get(i);
                                    wordList.add(new Word(o));
                                }


                                Comparator<Word> comparator = new Comparator<Word>() {
                                    @Override
                                    public int compare(Word lhs, Word rhs) {
                                        return lhs.getTur().length()-rhs.getTur().length();
                                    }
                                };
                                Collections.sort(wordList,comparator);

                                bilinen =0;
                                createGame();

                                int t = response.getInt("time");

                                if(t<=20){
                                    finish();
                                }

                                if (timer!=null){
                                    timer.cancel();
                                }

                                timer = new CountDownTimer(t * 1000, 1000) {
                                    @Override
                                    public void onTick(long l) {


                                        if(alive){
                                            if (Constant.pass && i < wordList.size()) {
                                                puan = puan + vPuan;//puanı ekle
                                                wordList.remove(i-1);
                                                i--;
                                                bilinen++;
                                                createGame();
                                            }





                                            if(clickCounter==0 && ortaLayout.getChildCount()>0){
                                                harfAlButton.setText("sil");
                                            }


                                            sec = (int) ((l / 1000)-20);

                                            txt.setText("" + sec);

                                            if(sec ==20){
                                                speakOut("son 20 saniye");
                                            }

                                            if(sec == 10){
                                                speakOut("son 10 saniye");
                                            }

                                            if ((l / 1000) == 20) {
                                                /**
                                                 * oyunu bitir
                                                 * */
                                                //SoundManeger.stopTicPlayer();

                                                JSONObject o = new JSONObject();
                                                try {


                                                    speakOut("Senin puanın "+puan);
                                                    o.accumulate(KEY_PUAN, puan);
                                                    o.accumulate(KEY_ANDROID_ID, Constant.getAndroid_id());

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                                getJson(o);
                                                ((LinearLayout) infoView.getParent()).setVisibility(View.INVISIBLE);
                                            }

                                            if ((l / 1000) == 10) {
                                                getScoreList();
                                            }
                                        }

                                    }

                                    @Override
                                    public void onFinish() {
                                        dialog.setCancelable(true);
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }

                                        if(alive){
                                            toZero();
                                            getWordListFromServer();
                                            ((LinearLayout) infoView.getParent()).setVisibility(View.VISIBLE);
                                        }else{
                                            finish();
                                        }
                                    }
                                }.start();



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        ApplicationController.getsInstance().addToRequestQueue(request);
    }


    public static LinearLayout getAltLayout() {
        return altLayout;
    }
}

