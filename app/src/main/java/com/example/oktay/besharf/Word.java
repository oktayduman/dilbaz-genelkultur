package com.example.oktay.besharf;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oktay on 04.10.2015.
 */
public   class Word {
    private String tur;
    private String eng;

    public Word(JSONObject o){
        try {
            setEng(o.getString("source").trim());
            setTur(o.getString("destin").trim());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getTur() {
        return tur;
    }

    public void setTur(String tur) {
        this.tur = tur;
    }

    public String getEng() {
        return eng;
    }

    public void setEng(String eng) {
        this.eng = eng;
    }

    public static List<String> stringToList(String word) {
        word = word.trim();
        String[] giden = word.split("");
        List<String> yeni = new ArrayList<String>();
        for (int j = 0; j < word.length(); j++) {
            yeni.add(giden[j + 1]);
        }
        return yeni;
    }

}
